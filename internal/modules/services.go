package modules

import (
	"gitlab.com/dvkgroup/goboilerplate/internal/infrastructure/component"
	aservice "gitlab.com/dvkgroup/goboilerplate/internal/modules/auth/service"
	uservice "gitlab.com/dvkgroup/goboilerplate/internal/modules/user/service"
	"gitlab.com/dvkgroup/goboilerplate/internal/storages"
)

type Services struct {
	User uservice.Userer
	Auth aservice.Auther
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	userService := uservice.NewUserService(storages.User, components.Logger)
	return &Services{
		User: userService,
		Auth: aservice.NewAuth(userService, storages.Verify, components),
	}
}
