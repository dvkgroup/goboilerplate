package modules

import (
	"gitlab.com/dvkgroup/goboilerplate/internal/infrastructure/component"
	acontroller "gitlab.com/dvkgroup/goboilerplate/internal/modules/auth/controller"
	ucontroller "gitlab.com/dvkgroup/goboilerplate/internal/modules/user/controller"
)

type Controllers struct {
	Auth acontroller.Auther
	User ucontroller.Userer
}

func NewControllers(services *Services, components *component.Components) *Controllers {
	authController := acontroller.NewAuth(services.Auth, components)
	userController := ucontroller.NewUser(services.User, components)

	return &Controllers{
		Auth: authController,
		User: userController,
	}
}
