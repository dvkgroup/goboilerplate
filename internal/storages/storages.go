package storages

import (
	"gitlab.com/dvkgroup/goboilerplate/internal/db/adapter"
	"gitlab.com/dvkgroup/goboilerplate/internal/infrastructure/cache"
	vstorage "gitlab.com/dvkgroup/goboilerplate/internal/modules/auth/storage"
	ustorage "gitlab.com/dvkgroup/goboilerplate/internal/modules/user/storage"
)

type Storages struct {
	User   ustorage.Userer
	Verify vstorage.Verifier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		User:   ustorage.NewUserStorage(sqlAdapter, cache),
		Verify: vstorage.NewEmailVerify(sqlAdapter),
	}
}
